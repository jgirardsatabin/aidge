Scheduler
=========


Sequential scheduler
--------------------

.. tab-set::

    .. tab-item:: Python

        .. autoclass:: aidge_core.SequentialScheduler
            :members:
            :inherited-members:

    .. tab-item:: C++
        
        .. doxygenclass:: Aidge::SequentialScheduler
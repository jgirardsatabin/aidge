Graph
=====

Node
----

.. tab-set::

    .. tab-item:: Python

        .. autoclass:: aidge_core.Node
            :members:
            :inherited-members:

    .. tab-item:: C++

        .. doxygenclass:: Aidge::Node

GraphView
---------

.. tab-set::

    .. tab-item:: Python

        .. autoclass:: aidge_core.GraphView
            :members:
            :inherited-members:

    .. tab-item:: C++

        .. doxygenclass:: Aidge::GraphView

Graph helper
------------

.. tab-set::

    .. tab-item:: Python

        .. autofunction:: aidge_core.sequential

    .. tab-item:: C++

        .. doxygenfunction:: Aidge::Sequential

.. tab-set::

    .. tab-item:: Python

        .. autofunction:: aidge_core.parallel

    .. tab-item:: C++

        .. doxygenfunction:: Aidge::Parallel

.. tab-set::

    .. tab-item:: Python

        .. autofunction:: aidge_core.residual

    .. tab-item:: C++

        .. doxygenfunction:: Aidge::Residual


Connector
---------

.. tab-set::

    .. tab-item:: Python

        .. autoclass:: aidge_core.Connector
            :members:
            :inherited-members:

    .. tab-item:: C++

        .. doxygenclass:: Aidge::Connector


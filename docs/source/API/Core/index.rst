Aidge core API
==============


.. toctree::
    :maxdepth: 2

    data.rst
    graph.rst
    operator.rst
    scheduler.rst
    graphMatching.rst
    recipies.rst
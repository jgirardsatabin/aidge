Quick Start
===========

.. warning::

    Fake example of quick start documentation, this section will be updated later on ...


Tensor example
--------------

First we need to create a : :ref:`Tensor <source/userguide/data:tensor>`.

.. tab-set::

    .. tab-item:: Python

        To instanciate a tensor you can use :py:class:`aidge_core.Tensor`:

        .. code-block:: Python
        
            aidge.Tensor()

    .. tab-item:: C++
        
        To instanciate a tensor you can use :cpp:class:`Aidge::Tensor`:

        .. code-block:: C++
        
            Aidge::Tensor()

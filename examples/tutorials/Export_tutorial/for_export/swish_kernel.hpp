#ifndef __AIDGE_EXPORT_CPP_KERNELS_SWISH__
#define __AIDGE_EXPORT_CPP_KERNELS_SWISH__

#include <type_traits>
#include "network/typedefs.hpp"
#include "network/utils.hpp"

template<int NB_OUTPUTS, 
         int OUTPUTS_HEIGHT, int OUTPUTS_WIDTH,
         typename Input_T, typename Output_T, typename Beta_T>
__attribute__((always_inline)) inline 
void swish_forward (
    const Input_T* inputs,
    Output_T* outputs,
    const Beta_T* betas)
{

}


#endif  // __AIDGE_EXPORT_CPP_KERNELS_SWISH__
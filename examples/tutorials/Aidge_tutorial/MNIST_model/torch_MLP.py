import torch
import torch.nn as nn
import torch.nn.functional as F

from torchvision import datasets, transforms

import numpy as np
import onnx
import argparse
import os

## Model
class M(torch.nn.Module):     
    def __init__(self):
        super().__init__()
        
        self.fc1 = torch.nn.Linear(28*28,50)
        self.fc1_drop = nn.Dropout(0.2)
        self.fc2 = torch.nn.Linear(50,50)
        self.fc2_drop = nn.Dropout(0.2)
        self.fc3 = torch.nn.Linear(50,10)

    def forward(self, x):
        # Flatten input
        x = torch.flatten(x,1)

        #FC1 - ReLU - Dropout
        x = self.fc1(x)
        x = F.relu(x)
        x = self.fc1_drop(x)

        #FC2 - ReLU - Dropout
        x = self.fc2(x)
        x = F.relu(x)
        x = self.fc2_drop(x)

        #FC3
        x = self.fc3(x)

        return x



def train(model, train_loader, epoch, optimizer, criterion, log_interval=200):
    # Set model to training mode
    model.train()
    
    # Loop over each batch from the training set
    for batch_idx, (data, target) in enumerate(train_loader):
        # Zero gradient buffers
        optimizer.zero_grad() 
        
        # Pass data through the network
        output = model(data)

        # Calculate loss
        loss = criterion(output, target)

        # Backpropagate
        loss.backward()
        
        # Update weights
        optimizer.step()
        
        if batch_idx % log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.data.item()))
            
def validate(model, validation_loader, criterion):
    model.eval()
    val_loss, correct = 0, 0
    for data, target in validation_loader:
        output = model(data)
        val_loss += criterion(output, target).data.item()
        pred = output.data.max(1)[1] # get the index of the max log-probability
        correct += pred.eq(target.data).cpu().sum()

    val_loss /= len(validation_loader)

    accuracy = 100. * correct.to(torch.float32) / len(validation_loader.dataset)
    
    print('\nValidation set: Average loss: {:.4f}, Accuracy: {}/{} ({:.2f}%)\n'.format(
        val_loss, correct, len(validation_loader.dataset), accuracy))

    return accuracy


def main():
    parser = argparse.ArgumentParser(description='PyTorch MNIST Example')
    parser.add_argument('--batch-size', type=int, default=32, metavar='N',
                        help='input batch size for training (default: 32)')
    parser.add_argument('--epochs', type=int, default=15, metavar='N',
                        help='number of epochs to train (default: 15)')
    parser.add_argument('--test', action='store_true', default=False,
                        help='test the model Best_mnist_MLP.pt if it exists')
    args = parser.parse_args()

    folder_path = os.path.dirname(os.path.abspath(__file__))
    data_path = os.path.join(folder_path, "data")
    model_path = os.path.join(folder_path, "Best_mnist_MLP.pt")
    onnx_path = os.path.join(folder_path, "MLP_MNIST.onnx")
    digit_path = os.path.join(folder_path, "digit")
    output_path = os.path.join(folder_path, "output_digit")

    trf=transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.1307,), (0.3081,))
            ])

    train_dataset = datasets.MNIST(data_path, 
                                train=True, 
                                download=True, 
                                transform=trf)

    validation_dataset = datasets.MNIST(data_path, 
                                        train=False, 
                                        transform=trf)

    train_loader = torch.utils.data.DataLoader(dataset=train_dataset, 
                                            batch_size=args.batch_size, 
                                            shuffle=True)

    validation_loader = torch.utils.data.DataLoader(dataset=validation_dataset, 
                                                    batch_size=args.batch_size, 
                                                    shuffle=False)
    
    
    
    ####### TRAIN ########

    model = M().cpu()
    optimizer = torch.optim.SGD(model.parameters(), lr=0.01, momentum=0.5)
    criterion = nn.CrossEntropyLoss()
    
    if args.test:
        os.path.isfile(model_path)
        model.load_state_dict(torch.load(model_path))
        validate(model, validation_loader, criterion)
    else:
        best_acc = 0
        for epoch in range(1, args.epochs + 1):
            train(model, train_loader, epoch, optimizer, criterion)
            acc = validate(model, validation_loader, criterion)
            if acc > best_acc:
                best_acc = acc
                print('New best accuracy : ', best_acc)
                torch.save(model.state_dict(), model_path)
                print('-------------- Best model saved --------------\n')


        # Load the best model
        model.load_state_dict(torch.load(model_path))

        # Find one digit correctly predicted by the best model
        found = False
        i=-1
        while not found:
            i+=1
            if i > train_dataset.__len__():
                raise RuntimeError('No correctly predicted digits')
            x, t = train_dataset.__getitem__(i)
            x = x.unsqueeze(0)
            out = model(x)
            pred = out.data.max(1)[1] # get the index of the max
            found = pred.eq(t)

        # Save digit & the model output
        output = model(x)

        np.save(digit_path, x.numpy())
        np.save(output_path, output.detach().numpy())

        ####### EXPORT ONNX ########
        torch.onnx.export(model, x, onnx_path, verbose=True, input_names=[ "actual_input" ], output_names=[ "output" ])


if __name__ == '__main__':
    main()


